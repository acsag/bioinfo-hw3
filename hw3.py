from Bio.PDB import *
from Bio import pairwise2
from Bio.SubsMat.MatrixInfo import blosum62

import aacids	


TESTFOLDER = 'testPairs'
PDB_PAIRS_LIST = 'pdblist.txt'

def pdb_files_exist():
	from os.path import isdir
	return isdir(TESTFOLDER)

def get_files():
	'''
		gets names of pdb files under 'testPairs' global directory
		returns a list of file tuples
	'''
	from os import walk
	from os.path import join
	global TESTDIR
	files = []
	for dirname, dirnames, filenames in walk(TESTFOLDER):
		newfiles = []
		for filename in filenames:
			f = filename.split('.')[0]
			newfiles.append( (f, join(dirname, filename)) )
		if newfiles:
			files.append(newfiles)
	return files


def read_pdb( pdb ):
	'''
		reads and parse pdb file to its amino acid sequence
		returns that string sequence
	'''
	parser = PDBParser()
	fname = pdb[0]		# pdb id
	pdb_loc = pdb[1]	# pdb location
	print "pdb:",fname, pdb_loc
	
	structure = parser.get_structure(fname, pdb_loc)
	sqnc = ''
	for model in structure:
		for chain in model:
			for residue in chain:
				if residue.resname in aacids.aa_dict: # ignore if a.acid is not known
					sqnc = sqnc + aacids.aa_dict[residue.resname] # append 1-letter-a.acid
	return sqnc
	

def locally_algn( sq1, sq2 ):
	'''
		does local alignment
		returns top alignment as tuple (al1, al2, score, begin, end)
	'''
	gap_open = -5
	gap_extend = 0
	
	
	alignments = pairwise2.align.localds(sq1, sq2, blosum62, gap_open, gap_extend)
	a = alignments[0]
	
	'''
	for a in alignments:
		print(pairwise2.format_alignment(*a))
	'''
	return a

def download_files(pdblist):
	'''
		downloads list of pdb id pairs
	'''
	from sys import exc_info
	
	plist = PDBList()
	try:
		f = open('./'+pdblist, 'r')
		for count_, line in enumerate(f):
			sq = line.split()
			for pdb_id in sq: # for each pair 'pdb_id's
				try:
					plist.retrieve_pdb_file(pdb_id.upper(), pdir='./'+TESTFOLDER+'/'+str(count_+1)+'/')
				except:
					print "CANNOT DOWNLOAD PDB FILE NAMED:",pdb_id
					print exc_info()
		
	except IOError:
		print "NO SUCH FILE!"
		if not f:
			f.close()
	finally:
		if not f:
			f.close()



# example run
#sq1 = 'LSREEIRKLDRDLRILVATNQDTPREELDRCQYSNDIDTRSGDRFVLHGRVFKN'
#sq2 = 'LSDEEIRKLNRDLRILIATNEDNSREEPIRHQRSVGTSARSGRSICT'
#print locally_algn(sq1,sq2)

if __name__ == "__main__":
	if not pdb_files_exist():
		download_files(PDB_PAIRS_LIST)
	files = get_files()

	for pair in files:
		sq1 = read_pdb(pair[0])
		sq2 = read_pdb(pair[1])
		""
		print '__________________seq1: _______________________'
		print sq1 
		print 
		print '*******************seq2: ***********************'
		print
		print sq2
		print
		print '___________________ local alignment: _______________________'
		""
		locally_algn(sq1,sq2)
		#break # $DEBUG


"""
		BASIC ALGORITHM that I used
	
	1- read pdb files in each folder under 'testPairs' folder
	2- for each sequences make local alignment using 'pairwise2' module from Biopy
	3- TODO: superimposition through jama code
		a) put the result into a file and run jama code from that file manually
		b) run java on python
	4- 
"""
